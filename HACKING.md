# Hacking

## Deploying in a local container for development

For convenience, this project has a `docker-compose.yml` file so you can use
`docker-compose` to bootstrap all services (db, api_server, and web client). So
in case you feel comfortable with Docker containers, just run the following
commands and wait a few minutes for the first pulling:


    $ docker-compose build --no-cache
    $ docker-compose up

However, if you don't like to use containers here, you can set up each service
manually.  Please visit each repository for details.

## Reporting Issues

If you found any issue during the bootstrap, please feel free to [Open a new
issue](https://gitlab.com/insights4ci/insights4ci/-/issues).

## Before submitting a MR

Make sure you have read our Contributor Guide at CONTRIBUTING.md, before
sending a MR.
